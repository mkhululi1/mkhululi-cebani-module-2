class App{
  String name;
  int year;
  App(this.name, this.year);

  String toString(){
    return' ${this.name}, ${this.year} ';
  }
}

void main() {
  List apps = [];
  apps.add(App("FNB",2012));
  apps.add(App("SnapScan",2013));
  apps.add(App("LIVE Inspect",2014));
  apps.add(App("WumDrop",2015));
  apps.add(App("Domestly",2016));
  apps.add(App("Shyft",2017));
  apps.add(App("Khula ecosystem",2018));
  apps.add(App("Naked Insurance",2019));
  apps.add(App("EasyEquities",2020));
  apps.add(App("Ambani Africa",2021));
  
  // a)
  apps.sort((a, b) => a.name.compareTo(b.name));
  for (var app in apps){
      print(app.toString());
  }

  // b)
  for (var app in apps){
    if(app.year == 2017 || app.year == 2018){
      print(app);
    }
  }
  
  // c)
  print(apps.length);
}
