class App{
  String name;
  int year;
  String category;
  String developer;

  App(this.name, this.year, this.category, this.developer);

  String toString(){
    return' ${this.name}, ${this.year}, ${this.category}, ${this.developer} ';
  }
  String toCaps(){
    return ' ${this.name.toUpperCase()}';
  }
}

void main() {
  var apps = [];
  apps.add(App("FNB",2012, "Best Financial Solution", "Unknown"));
  apps.add(App("SnapScan",2013,"Best Financial App", "Kobus Ehlers"));
  apps.add(App("LIVE Inspect",2014,"Best Android App (Enterprise)", "Unknown"));
  apps.add(App("WumDrop",2015, "Best Enterprise App", "Simon Hartley"));
  apps.add(App("Domestly",2016,"Best Consumer App", "Berno Potgieter"));
  apps.add(App("Shyft",2017," Best Breakthrough Developer accolade","Unknown"));
  apps.add(App("Khula ecosystem",2018,"Best Agriculture Solution" , "Karidas Tshintsholo."));
  apps.add(App("Naked Insurance",2019, "Best Financial Solution Award", "Alex Thompson"));
  apps.add(App("EasyEquities",2020,"Best Consumer Solution category", "The Purple Groug"));
  apps.add(App("Ambani Africa",2021,"Best Educational Solution","Mukundi Lambani"));


  for(var app in apps){
    print(app.toString());
  }

  for(var app in apps){
    print(app.toCaps());
  }
 
}
